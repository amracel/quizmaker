import React from "react";
import { Route, Routes, Link } from "react-router-dom";
import QuizList from "./components/quizlist";
import Quiz from "./components/quiz";
import ErrorBoundary from './api/ErrorBoundary';

function Home() {
    return <QuizList/>;
}

function Menu() {
    return (<p><Link className="btn btn-link" to={`/`}>Return to Menu</Link></p> )
}


function App() {
    return (
        <ErrorBoundary>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="questions/:id" element={<Quiz />} />
            </Routes>
            <Menu/>
        </ErrorBoundary>
    );
}

export default App;
