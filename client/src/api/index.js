const API_BASE_URL = process.env.API_BASE_URL || "http://192.168.1.159:5000";

/*
Fetch json from given url. Handle errors.
 */
async function fetchJson(url, options, onCancel) {
    try {
        const response = await fetch(url, options);

        if(response.status < 200 || response.status > 399) {
            throw new Error(`${response.status} - ${response.statusText}`);
        }
        if(response.status === 204) {
            return null;
        }
        return await response.json();
    } catch(error) {
        if(error.name !== "AbortError") {
            console.error(error.stack);
            throw error;
        }
        return Promise.resolve(onCancel);
    }
}

/*
Retrieves names of all quizzes available
 */
export async function listQuizzes(signal) {
    const url = API_BASE_URL + "/quiz";
    console.log(url);
    return await fetchJson(url,{signal},{});
}

/*
Retrieves the questions for a specific quiz
 */
export async function readQuiz(quizId, signal) {
    let url = API_BASE_URL + `/questions/${quizId}`;
    url = url.replace(/ /g, '%20');
    console.log(url);
    return await fetchJson(url, { signal },{});
}
