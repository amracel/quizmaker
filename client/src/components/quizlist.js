import React, {useEffect, useState} from "react";
import { Link } from "react-router-dom";
import { listQuizzes } from "../api";


export default function QuizList() {
    const [records, setRecords] = useState([]);

    // This method fetches the records from the database.
    useEffect(() => {
            const abortController = new AbortController();

            async function fetchQuizList() {
                console.log("fetching records");
                const response = await listQuizzes(abortController.signal);
                setRecords(response);
                
            }

            fetchQuizList();
        }
        ,
        [records.length]
    );

    function quizList() {
        console.log(records);
        return records.map((record) =>
            <li><Link className="btn btn-link" to={`/questions/${record}`}>{record}</Link></li>
        )
    }

    return (
        <div className='app'>
            <ul>
        {quizList()}
            </ul>
        </div>
    );
}
