import React, {useEffect, useState} from 'react';
import {Link, useParams} from "react-router-dom";
import {readQuiz} from "../api";
import {Watch} from 'react-loader-spinner';


export default function Quiz() {
    const [questions, setQuestions] = useState([]);
    const quizId = useParams().id;
    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [showScore, setShowScore] = useState(false);
    const [score, setScore] = useState(0);
    const [loading, setLoading] = useState(true);
    const [submitButton, setSubmitButton] = useState("Submit");
    const [checkedState, setCheckedState] = useState(
        new Array(4).fill(false)
    );

    const handleOnChange = (position) => {
        const updatedCheckedState = checkedState.map((item, index) =>
            index === position ? !item : item
        );
        setCheckedState(updatedCheckedState);
    }
    console.log(quizId);

    useEffect(() => {
            console.log("prefetch questions");
            const abortController = new AbortController();

            async function fetchQuestions() {
                console.log("fetching questions");
                const response = await readQuiz(quizId, abortController.signal);
                console.log(response);
                setQuestions(response);
                setLoading(false);
            }

            fetchQuestions();
        }
        ,
        [questions.length]
    );

    function handleSubmit(event) {
        event.preventDefault();
        console.log("event",event);
        if(submitButton === "Submit") {

            let increment = true;

            // make sure all the answers are correct
            for (let i = 0; i < checkedState.length; i++) {
                const checkboxAnswer = document.querySelector(`#answer-${i}`);
                if (questions[currentQuestion].correct.includes(i)) {
                    checkboxAnswer.setAttribute("class","correct");
                    if (checkedState[i] === false) {
                        increment = false;
                    }
                } else {
                    if (checkedState[i] === true) {
                        increment = false;
                    }
                }
            }
            // if it was correct, add a point
            if (increment) {
                setScore(score + 1);
            }

            setSubmitButton("Continue");
        } else {
            const nextQuestion = currentQuestion + 1;
            console.log("question",nextQuestion);
            if (nextQuestion < questions.length) {
                setCurrentQuestion(nextQuestion);
            } else {
                setShowScore(true);
            }
            for (let i = 0; i < checkedState.length; i++) {
                const checkboxAnswer = document.querySelector(`#answer-${i}`);
                checkboxAnswer.setAttribute("class", "standard");
            }
            setCheckedState(new Array(4).fill(false));
            setSubmitButton("Submit");
        }

    }

    if (loading) {
        return (
            <Watch type='Circles' color='#00BFFF' height={80} width={80}/>
        )
    } else {
        return (
            <div className='app'>
                {showScore ? (
                    <div className='score-section'>
                        You scored {score} out of {questions.length}
                    </div>
                ) : (
                    <div className='quiz'>
                        <form onSubmit={handleSubmit}>
                            <div className='question-section'>
                                <div className='question-count'>
                                    <span>Question {currentQuestion + 1}</span>/{questions.length}
                                </div>
                                <div className='question-text'>{questions[currentQuestion].question}</div>
                            </div>
                            <div className='answer-section'>
                                {questions[currentQuestion].answers.map((answerOption, index) => (
                                    <div key={`answer-${index}`} id={`answer-${index}`} className='standard'>
                                        <input
                                            type="checkbox"
                                            id={`custom-checkbox-${index}`}
                                            name="response"
                                            checked={checkedState[index]}
                                            onChange={() => handleOnChange(index)}
                                        />
                                        <label htmlFor={`custom-checkbox-${index}`}>{answerOption}</label>
                                    </div>
                                ))}
                            </div>
                            <br/>

                            <input type="submit" value={submitButton} className='submit'/>

                        </form>
                    </div>
                )}
            </div>
    )
    }
}
